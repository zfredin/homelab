# homelab

The MAS.863 2020 homelab kit consists of large things, which don't have a dedicated storage space, and small things, which fit in a black plastic case.

---

## large things

### vacuum
A portable rechargeable vacuum cleaner in a zippered case. Includes various attachments and hoses, along with a USB charging adapter (supply your own wall adapter). Primarily used for cleaning up after [Clank](https://gitlab.cba.mit.edu/jakeread/clank), but handy for general tidiness tasks as well.

![vacuum](img/vacuum.jpg)

### vise
A sturdy work-holding device for keeping circuit boards stationary during soldering. Loosen the middle handle to adjust the ball joint angle as needed. The lower handle engages the triangular suction cup on the base, securing the vise to a non-porous surface.

![vise](img/vise.jpg)

### soldering/reflow station
A combination reflow and soldering station. The reflow side sends hot air out of the wand, which can be used to remove components from boards. Use the included clamp-on reducers to concentrate airflow for small parts, and adjust the temperature and velocity as needed. The soldering iron is temperature-controlled and suitable for surface-mount and through-hole soldering. This tool gets hot during use and remains hot after you shut it off, so be careful and always leave it in a safe state.

![solderingstation](img/solderingstation.jpg)

The specific model in the kit is the 110 VAC / 60 Hz version of the [Yihua 8786D](http://yihua-soldering.com/product-1-2-5-hot-air-rework-station-en/147661/). The system has an on-off switch on the back; once you turn this on, the hot air (left) and iron (right) functions can be activated with their own dedicated switches. Before turning the station on, ensure that the iron is securely plugged in to the front of the station using the included locking cable. If you like, you can secure the iron holder to the top of the station using the two screws near the back of the case.

The irons ship with a sharp conical tips; while these may seem ideal for small surface mount components, the small surface area results in slow heat transfer between iron tip and part. We included a 1/16" chisel tip in each kit; when the iron is cool, unscrew the locking collar near the handle and carefully slip off the retaining tube to switch the tip out. Do this carefully as the heating element is somewhat delicate. We recommend trying both and using whichever you prefer. Remember to tin your iron tip as soon as it heats up.

### fume extractor
An exhaust fan with a front-mounted activated carbon filter, designed to absorb soldering fumes. Use whenever soldering; be sure it is close enough that visible fumes enter filter. Remove activated carbon filter from plastic wrap before use.

![fumeextractor](img/fumeextractor.jpg)

### clip light
A rechargeable LED clip light. On-off touch switch with three brightness levels located on clip. Useful for documentation and general work illumination. USB charging cable included (supply your own wall adapter).

![cliplight](img/cliplight.jpg)

---

## small things

### case
All of the small fussy parts come in a black plastic case. Consider decorating your case, perhaps with the help of the [vinyl cutter](http://academy.cba.mit.edu/classes/computer_cutting/index.html).

![case_closed](img/case_closed.jpg)

The case features a removable foam insert with cutouts for some items; larger things are stored opposite the insert. Yes, everything does fit if you put it all back carefully.

![case_opened](img/case_opened.jpg)

### soldering supplies
From the top left and clockwise: iron tip cleaner; solder wire; desoldering braid; flux pen; FR1 PCB blanks; silicone heat-resistant work surface (white, somewhat difficult to see); safety glasses.

![solderingsupplies](img/solderingsupplies.jpg)

The solder originally supplied with the irons wasn't great, so we swapped it for 0.020" Chip Quik SAC305 rosin-core no-clean solder. SAC305 is a lead-free alloy, so if you've used 63/37 or 60/40 leaded solder in the past you may find you need to increase the station temperature a bit. A value between 350 and 400 C (note that the temp readout is in C!) is a good starting point. Use the flux pen as needed to improve solder wetting; often a small dab on a pad will dramatically improve joint quality and speed. A bit of flux on the desoldering braid also goes a long way if you're having trouble removing a bridge or desoldering a bad part.

### hand tools
From left to right, then top to bottom: a USB logic analyzer; two small clamps; a digital caliper; a multimeter; a stainless steel rule; a box cutter; a pair of tweezers; flush cutters; needle-nose pliers; and a metric hex key set. Not shown, a DSO Nano v3 pocket oscilloscope and mini-USB charging cable (supply your own wall adapter).

![handtools](img/handtools.jpg)

The multimeter includes a 9 volt battery that you'll need to install by removing a single screw on the back of the case. The pocket oscilloscope is documented [here](https://wiki.seeedstudio.com/DSO_Nano_v3/), and includes a tiny stand, two probes, a padded carrying case, and a hex key for disassembly (in case you're curious about how it works). Keep the caliper in its case with the spare battery to avoid damage.

There is a good deal more information related to test equipment [here](testequipment.md).

### documentation
Project documentation is incredibly important! For good photos, we included a ~1 m^2 sheet of bright white backdrop material (used in most of these pictures), along with a roll of tape to attach it to your wall/chair/desk. The tripod and phone adapter can be used for making videos or taking photos. The USB microscope will help diagnose problems with electronic circuits.

![documentation](img/documentation.jpg)

### electronics
Includes red insulated hook-up wire; a small 8-ohm paper speaker; ribbon cable; alligator clip leads; jumpers; FR1 PCB substrates (also seen in the "soldering supplies" section); conductive tape; and a pile of components, described in detail [here](components.md). The components come in individual bags on tape; to save space and make finding things easier, consider labeling each tape with a pen and store it in the included plastic sleeves, or in the grid of tiny plastic bins. Use the USB microscope to identify parts that aren't labeled (or ask your helpful TAs).

The vendor that supplied these kits, [Seeed Studio](https://www.seeedstudio.com/), also included one of their Grove starter kits that is filled with goodies. Thanks Seeed!

![electronics](img/electronics.jpg)
