## Test Equipment
Believe it or not, the jumble of tiny electronic instruments in your black case would have cost a few thousand dollars a decade ago. The popularity of home electronics and the ubiquity of fast embedded processors have created a market for simple, portable, low-cost test instruments that give you a great deal of power to understand how your circuit works. Or, as the case may be, _doesn't_ work.

### [Multimeter](https://en.wikipedia.org/wiki/Multimeter)
A multimeter is the first line of defense against circuit gremlins. Your multimeter needs a 9V battery to work; unscrew the single Philips head screw on the back and pop in the included battery, then button the case back up. If you like, peel off the clear plastic sticker protecting the LCD display.

#### continuity testing
Continuity testing applies a small voltage to a circuit through the test probes. If the voltage completes its loop through the test article, the meter beeps and displays the resistance value. Continuity testing is how you check solder joints and other electrical connections, and can also be used to find short circuits, solder bridges, and cut traces. It's not a bad idea to use the continuity testing function on your bare PCB before stuffing it with parts, to be sure the right pads connect to each other and aren't shorted to other pads. When you perform continuity tests, make sure your circuit is not powered; set the meter to the diode/noise setting between 200 Ohm and 10 A; and connect the leads to COM (black) and mAVOhm (red):

![continuity](img/continuity.jpg)

Note that you can also use the continuity function to test LED polarity, as I show towards the end of this video:

![continuity](img/continuity.mp4)

#### voltage testing
The multimeter can be used to check DC voltage, to verify power is getting to a chip, check a slow-moving analog signal, diagnose a power supply issue, etc. The various DC voltage measurement ranges are directly clockwise from the OFF position; generally, you should use the closest range that you know will clear the maximum voltage you're expecting to measure. As with continuity testing, the black lead goes to COM while the red lead goes to mAVohm:

![voltage](img/voltage.jpg)

This video shows a few common voltage measurements you might perform on a circuit:

![voltage](img/voltage.mp4)

#### other functions
You can check resistance values using the Ohm settings, directly counter-clockwise from the OFF position. Like continuity testing, make sure the power is off when performing these tests, since the meter supplies a small voltage to make the measurement. Resistance tests are handy for verifying the value of a component before soldering it down, or checking for high resistance connections at solder joints, screw terminals, or header connections.

Current measurement can be used to test circuit efficiency by measuring the amps flowing through your PCB. Measuring current requires that you wire the multimeter _in series_ with your circuit; don't measure current like you would voltage, since this would short out the meter and potentially damage it (or at least blow the internal fuse).

We don't recommend using your meter for AC voltage measurement. Don't mess with line voltage.

### [Oscilloscope](https://en.wikipedia.org/wiki/Oscilloscope)
The Seeed Studio DSO Nano v3 is officially documented [here](https://wiki.seeedstudio.com/DSO_Nano_v3/). It's worth reading through the manufacturer's documentation, but some of the screen images aren't current and the guide generally assumes familiarity with oscilloscopes: triggering, probes, voltage scaling, time bases, etc.

Oscilloscopes allow you to visualize changing voltages in the time domain. Some signals repeat consistently, so the 'scope simply shows the same waveform over and over; the _trigger_ function ensures that it redraws the screen in the same spot each time. Other signals are sporadic or short-lived, so a _digital storage oscilloscope_ (like your DSO Nano) allows you to freeze the waveform, look back through a short history, or trigger once and stop in single-shot mode. You can use oscilloscopes for a variety of tasks: seeing if a GPIO line is toggling at the right frequency, checking the shape of an analog waveform, characterizing the noise on an input line, or seeing how much a [switch bounces](https://www.pololu.com/docs/0J16/4) when pressed and released.

Your DSO Nano comes with a few accessories: a padded case, for storage; a bent sheet metal bracket, to hold the screen at a nice viewing angle on the desk; a mini-USB cable for charging and data transfer (packaged separately, in the Clank box or in the black case); and two probes, one with black and white mini-grabbers and the other with header connections.

#### using your 'scope
Turn the DSO Nano on using the slide switch on the lower right side. After the instrument powers up and initializes, you'll be presented with a somewhat overwhelming array of colored lines and text:

![scope](img/scope.jpg)

The top row, from left to right, tells you triggering mode; Y-axis volts per divsion; probe multiplier (x1 or x10); X-axis seconds per division; stored data display mode; trigger mode (rising or falling); trigger sensitivity; and battery status. When one of these values is highlighted by a flashing cursor, you can use the left and right buttons to change it.

The right row holds menus that let you adjust categories of parameters. From top to bottom: Y-axis (voltage) settings; X-axis (time) settings; trigger settings; measurement mode; data display settings; data _storage_ settings; waveform generator settings; and calibration settings. You can use the up and down buttons to select these menus, then the OK button to view and adjust its parameters.

The bottom row shows you various other less-used parameters, like the waveform generator frequency, a waveform search bar for stored data, and the measurement mode output.

The center window, the bit with the black background, shows the actual waveform you're observing in blue; the stored waveform in pink; the trigger level in green; the T=0 point in orange; and X/Y cursors, used for measurements, in white.

When you first turn on your 'scope, it's not a bad idea to go through and turn off a bunch of the lines that clog up the display, leaving only the blue signal line for now:

![scope_clearscreen](img/scope_clearscreen.mp4)

Measuring a signal is simple! Connect the probe to your circuit and adjust settings to trigger correctly and display the part of the waveform you want to observe. Here is a quick demo video, showing how to connect the probes to a test PCB and observe a square wave:

![scope_squarewave](img/scope_squarewave.mp4)

You can also capture complex waveforms, like serial commands, using the single-shot trigger mode:

![scopeserial](img/scopeserial.mp4)

### [Logic Analyzer](https://en.wikipedia.org/wiki/Logic_analyzer)
Oscilloscopes are versatile tools for viewing analog and digital waveforms, but sometimes you want to look at several digital signals at once. When you're debugging issues with UART, SPI, or I2C lines, it's also handy to have a platform that automatically decodes signals into plain text, so you don't have to manually count waveforms to verify proper signaling. Your kit includes a small 8-channel 24-MHz USB logic analyzer for this purpose:

![logicanalyzer](img/logicanalyzer.jpg)

The logic analyzer uses an open-source program called Sigrok PulseView, which you can download [here](https://sigrok.org/wiki/Downloads). The [PulseView manual](https://sigrok.org/doc/pulseview/unstable/manual.html) is also quite informative, covering software installation, setup, data capture, and basic signal decoding. When setting up the device, select `fx2lafw (generic driver for FX2 based LAs)` via USB interface. Here is a quick demonstration decoding a serial UART signal generated by a microcontroller board, along with a screenshot of PulseView decoding the signal:

![logicanalyzer](img/logicanalyzer.mp4)

![pulseview](img/pulseview.png)

### USB [Microscope](https://en.wikipedia.org/wiki/Microscope)
Electronics is a world of tiny things! You're soldering tiny components to tiny boards with tiny bits of solder, using a tiny soldering iron tip. The USB microscope is provided as an inspection tool, both for yourself, and for us to help when trying to remotely diagnose problems with PCBs. To illustrate the power of the microscope, here's the best close-up image of my PCB I could get with my computer's built-in webcam:

![closeup_webcam](img/closeup_webcam.jpg)

... my phone:

![closeup_phone](img/closeup_phone.jpg)

... and the USB microscope:

![closeup_microscope](img/closeup_microscope.jpg)

The microscope has a few buttons on the side that interface with the included software that I didn't install; instead, I used a Linux program called GTK+ UVC Viewer to capture the image, since the microscope enumerates as a standard webcam. Adafruit sells a similar microscope and points users to [this website](https://plugable.com/drivers/microscope/) for various software packages that can capture images from webcams. The confusingly labeled '50x< >500x' knob actually adjusts the camera's focus; you can then "zoom in and out" by manually moving the camera closer or further from your work. This actually works surprisingly well; zoomed all the way out, the camera provides plenty of working distance for soldering. And the onboard LEDs can be adjusted using the knob built in to the USB cable. If you want to perform accurate measurements using the microscope, take reference images using the included calibration card and then measure things using [ImageJ](https://imagej.nih.gov/ij/).

Tip: if your images seem blurry or washed-out, you probably forgot to take off the clear protective lens over!

### Digital [Calipers](https://en.wikipedia.org/wiki/Calipers)
We included a set of digital calipers in each kit because they're fantastically useful for all kinds of mechanical design. Often, you need to design custom parts around existing components, and not all manufacturers provide detailed 3D CAD files for easy integration into your design tool. Calipers are a great quick way to determine critical dimensions of parts for a model. You can also use them to check tolerances on a manufacturing process, such as laser cutting or 3D printing.

![calipers](img/calipers.jpg)

Your calipers aren't particularly fancy, but they'll give you 0.01 mm resolution measurements across a range of 150 mm. Accuracy is probably in the range of +/- 0.1 mm, give or take; they aren't precision micrometers, but they're a whole lot better than the stainless steel rule (also included in your kit). Note that they come in a green case with a spare LR44 battery: calipers are precision mechanical metrology instruments and should be treated with care, so always keep them in the case when not in use.

Your calipers can be used three ways! You can measure the outer diameter of objects using the long jaws:

![calipers_od](img/calipers_od.jpg)

... inner dimensions using the short jaws:

![calipers_id](img/calipers_id.jpg)

... and even blind depths, using the depth bar at the end of the instrument:

![calipers_depth1](img/calipers_depth1.jpg)

![calipers_depth2](img/calipers_depth2.jpg)

Whenever you turn on your calipers, it's a good idea to gently close the jaws and hit the yellow `ZERO` button to ensure an accurate measurement. Make sure the jaws are clean!
