## passives
resistors: size 1206; 0, 47, 470, 1k, 4k7, 47k, 100k, 470k, 1M, 10M

capacitors: size 1206; 100p, 1n, 10n, 0u1, 1u, 10u

## actives
[small schottkey diodes](https://www.digikey.com/product-detail/en/comchip-technology/CDBM1100-G/641-1331-1-ND/2021252), [small N-channel MOSFETs](https://www.digikey.com/product-detail/en/on-semiconductor/NDS355AN/NDS355ANCT-ND/459000), [large N-channel MOSFETs](https://www.digikey.com/product-detail/en/on-semiconductor/RFD16N05LSM9A/RFD16N05LSM9ACT-ND/965715), [H-bridges](https://www.digikey.com/products/en?keywords=620-1400-1-ND), [small 3v3 LDOs](https://www.digikey.com/product-detail/en/texas-instruments/LM3480IM3-3-3-NOPB/LM3480IM3-3-3-NOPBCT-ND/270750), [large 3v3 LDOs](https://www.digikey.com/products/en?keywords=ZLDO1117G33DICT-ND), [small 5 V LDOs](https://www.digikey.com/product-detail/en/texas-instruments/LM3480IM3-5-0-NOPB/LM3480IM3-5-0-NOPBCT-ND/270751), [large 5 V LDOs](https://www.digikey.com/products/en?keywords=NCP1117ST50T3GOSCT-ND)

## output
size 1206 LEDs: [red](https://www.digikey.com/products/en?keywords=160-1167-1-ND), [green](https://www.digikey.com/products/en?keywords=160-1169-1-ND), [IR](1080-1346-1-ND)

[RGB LED](https://www.digikey.com/products/en?keywords=CLV1A-FKB-CK1VW1DE1BB7C3C3CT-ND)

[speaker](https://www.digikey.com/products/en?keywords=458-1136-ND)

## input
phototransistors, size 1206: [visible](https://www.digikey.com/products/en?keywords=1080-1380-1-ND), [IR](https://www.digikey.com/products/en?keywords=1080-1379-1-ND)

[analog MEMS microphone](https://www.digikey.com/products/en?keywords=423-1134-1-ND), [NTC thermister](https://www.digikey.com/products/en?keywords=235-1109-1-ND), [Hall effect sensor](https://www.digikey.com/products/en?keywords=620-1402-1-ND), [switches](https://www.digikey.com/products/en?keywords=SW262CT-ND)

## microcontrollers
[ATtiny412](https://www.digikey.com/product-detail/en/microchip-technology/ATTINY412-SSNR/ATTINY412-SSNRCT-ND/8594971), [ATtiny1614](https://www.digikey.com/product-detail/en/microchip-technology/ATTINY1614-SSNR/ATTINY1614-SSNRCT-ND/7354424), [ATtiny3216](https://www.digikey.com/product-detail/en/microchip-technology/ATTINY3216-SFR/ATTINY3216-SFRCT-ND/9477802)

[ATSAMD11C14](https://www.digikey.com/product-detail/en/microchip-technology/ATSAMD11C14A-SSNT/ATSAMD11C14A-SSNTCT-ND/5956608), [ATSAMD11D14](https://www.digikey.com/product-detail/en/microchip-technology/ATSAMD11D14A-SSUT/ATSAMD11D14A-SSUTCT-ND/5226483), [ATSAMD21E17](https://www.digikey.com/product-detail/en/microchip-technology/ATSAMD21E17D-AF/ATSAMD21E17D-AF-ND/9695753)

## networking
[ESP32-WROOM-32](https://www.espressif.com/sites/default/files/documentation/esp32-wroom-32_datasheet_en.pdf), [ESP01](https://www.mouser.com/ProductDetail/SparkFun/WRL-13678?qs=WyAARYrbSnZdmwzlRTs1Tw%3D%3D&gclid=EAIaIQobChMInLzu75S86wIVGW-GCh07ngmPEAQYASABEgKquvD_BwE), [RN4871](https://www.mouser.com/ProductDetail/Microchip-Technology/RN4871-I-RM130?qs=MLItCLRbWswoZsrzvVbGYw%3D%3D&gclid=EAIaIQobChMIvp7OhuG26wIVUeWGCh2W6gMQEAQYASABEgJC5fD_BwE)

## connectors
female insulation-displacement: 4 and 6 pin 0.1", 4 and 10 pin 0.05"

m/f header, barrel jack, screw terminals

## misc
copper tape, alligator clip wires, female-to-female header wires, 22 AWG wire, ribbon cable, magnets, [FTDI cable](https://www.amazon.com/Converter-Terminated-Galileo-BeagleBone-Minnowboard/dp/B06ZYPLFNB/ref=asc_df_B06ZYPLFNB/), single- and double-sided copper-clad FR1 PCB blanks
