## deltas
updates from 863.20 feedback and experience

plz add comments in the issue tracker.

## tools and materials
- add wire strippers
- hex keys --> ball end hex keys
- add Philips screwdriver
- include better soldering iron tip: fine point + chisel
- bad solder --> ChipQuik SAC305
- get rid of solder tip cleaner (built in to station)
- micro-USB charging cable from clip light --> data cable
- delete mills and engraving bits (include with Clank)
- delete vacuum (include with Clank)
- delete clamps (for Clank)
- better FTDI knockoff (5 VDC power, std pinout)
- add ARM-CMSIS programmer
- more FR1
- masking tape --> Kapton tape
- silicone mat --> cutting mat?
- add USB extension cable
- USB camera + 10x loupe
- add room temp solder paste
- wall wart of powering projects

## components
- Pi 4 for everything?
- pre-sort and label nicely
    - resistor value confusion (ex '474' = 470k, '471' = 470 ohm)
    - difficult to distinguish photodiodes, LEDs
- ensure hello-worlds are all build-able
- add small DC gear motor
- add small servo
- add stepper motor
- delete barrel jack
- downselect microcontrollers: more of fewer parts?
    - ATtiny412, ATtiny1614, SAMD11C, ATSAMD21G18, ESP32
    - Bluetooth module? same or different? or eliminate?
- include ESP32-CAM
- think through headers, connectors, and jumper wires so they match and can do everything needed
- add on-off slide switch
- add OLED
- add accelerometer
